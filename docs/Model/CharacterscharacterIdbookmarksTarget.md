# CharacterscharacterIdbookmarksTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**coordinates** | [**\ESI\Client\Model\CharacterscharacterIdbookmarksTargetCoordinates**](CharacterscharacterIdbookmarksTargetCoordinates.md) |  | [optional] 
**item** | [**\ESI\Client\Model\CharacterscharacterIdbookmarksTargetItem**](CharacterscharacterIdbookmarksTargetItem.md) |  | [optional] 
**location_id** | **int** | location_id integer | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


