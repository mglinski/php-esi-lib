# GetCharactersCharacterIdPlanetsPlanetIdOkContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **int** | content integer | [optional] 
**content_quantity** | **int** | content_quantity integer | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


