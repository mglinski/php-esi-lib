# GetCharactersCharacterIdClonesOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**home_location** | [**\ESI\Client\Model\GetCharactersCharacterIdClonesOkHomeLocation**](GetCharactersCharacterIdClonesOkHomeLocation.md) |  | [optional] 
**jump_clones** | [**\ESI\Client\Model\GetCharactersCharacterIdClonesOkJumpClones[]**](GetCharactersCharacterIdClonesOkJumpClones.md) | jump_clones array | 
**last_jump_date** | [**\DateTime**](\DateTime.md) | last_jump_date string | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


