# GetCharactersCharacterIdPlanetsPlanetIdOk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**\ESI\Client\Model\GetCharactersCharacterIdPlanetsPlanetIdOkLinks[]**](GetCharactersCharacterIdPlanetsPlanetIdOkLinks.md) | links array | 
**pins** | [**\ESI\Client\Model\GetCharactersCharacterIdPlanetsPlanetIdOkPins[]**](GetCharactersCharacterIdPlanetsPlanetIdOkPins.md) | pins array | 
**routes** | [**\ESI\Client\Model\GetCharactersCharacterIdPlanetsPlanetIdOkRoutes[]**](GetCharactersCharacterIdPlanetsPlanetIdOkRoutes.md) | routes array | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


