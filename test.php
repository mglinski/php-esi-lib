<?php
use ESI\Client\ApiException;
use ESI\Client\HTTPlugApiClient;

require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new ESI\Client\Api\AllianceApi(new HTTPlugApiClient());

$datasource = "tranquility"; // string | The server name you would like data from

try {
    $result = $api_instance->getAlliances($datasource);
    var_dump($result);
} catch (ApiException $e) {
    echo 'Exception when calling AllianceApi->getAlliances: ', $e->getResponseHeaders()[0], ' "', $e->getResponseBody()->error, '"', PHP_EOL;
}
